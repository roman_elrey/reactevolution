class Evolver {

    constructor(width, height, cells) {
        this.width = width;
        this.height = height;
        this.cells = cells;
        this.directions = [{x: 0, y: -1}, {x: 1, y: -1}, {x: 1, y: 0}, {x: 1, y: 1}, {x: 0, y: 1}, {x: -1, y: 1}, {x: -1, y: 0}, {x: -1, y: -1}];
    }

    countNeighbours(x, y,) {
        let neighbours = 0;
        for (let i = 0; i < 8; i++) {
            let dir = this.directions[i];
            let dirX = x + dir.x;
            let dirY = y + dir.y;
            if (dirX >= 0 && dirX < this.width && dirY >= 0 && dirY < this.height) {
                let index = dirX + (dirY * this.width);
                neighbours += this.cells[index] ? 1 : 0;
            }
        }
        return neighbours;
    }

    evolve() {
        let size = this.width * this.height;

        let newGrid = new Array(size);
        for (let i = 0; i < size; i++) {
            let x = i % this.width;
            let y = (i - x) / this.width;
            let neighbours = this.countNeighbours(x, y, this.width, this.height, this.cells);
            if (neighbours < 2 || neighbours > 3) {
                newGrid[i] = false;
            } else if (neighbours === 2) {
                newGrid[i] = this.cells[i];
            } else {
                newGrid[i] = true;
            }
        }

        this.cells = newGrid;
    }

    getCells() {
        return this.cells;
    }
}

export default Evolver;