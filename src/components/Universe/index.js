import React, {PureComponent} from 'react'
import './style.css'
import Evolver from '../Evolver'

export default class Universe extends PureComponent {
    state = {
        cells: this.props.cells,
        interval: this.props.interval,
        openArticleId: null,
    };

    render() {

        const cellElements = this.state.cells.map((cell, index) =>
            <label key={index} className={[cell ? 'alive ' : 'dead ', index % this.props.width === 0 ? ' last' : '']}>

            </label>
        );

        return (
            <div>
                <ul>
                    {cellElements}
                </ul>
            </div>
        )
    }

    componentDidMount() {
        this.interval = setInterval(() => {

            let evolver = new Evolver(this.props.width, this.props.height, this.state.cells);

            evolver.evolve();

            this.setState({cells: evolver.getCells()});

        }, this.state.interval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
}