import React, {PureComponent} from 'react'

import Universe from './Universe'

import CellGenerator from './CellGenerator'

import 'bootstrap/dist/css/bootstrap.css'

class App extends PureComponent {

    state = {
        reverted: false,
        width: 50,
        height: 25,
        interval: 75,
        startDensity: 0.25
    };


    render() {

        let cells = new CellGenerator(this.state.width, this.state.height, this.state.startDensity).generate();

        return (
            <div>
                <Universe cells={cells} width={this.state.width} height={this.state.height} interval={this.state.interval} />
            </div>
        )
    }

}

export default App