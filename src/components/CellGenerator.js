class CellGenerator {
    constructor(width, height, density) {
        this.width = width;
        this.height = height;
        this.density = density;
    }

    generate() {
        let cells = [];
        let size = this.width*this.height;
        for(let i=0; i<size; i++) {
            cells.push(Math.random() < this.density);
        }
        return cells;
    }
}

export default CellGenerator;